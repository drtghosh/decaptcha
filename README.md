Documentation for deCaptcha

CSE Captcha


CC Captcha
	Set of approaches taken into consideration 

		1. At first we assumed that through edge detection we can just remove the boundaries but the images produced by this method had a lot of noise.

		3. Another assumption was that the boundry of a character was darker than the inside color of a character. So from the RGB values, we calcualated the HSV value and just took the V channel and thresholded the image for a different V values. This produced somewhat good results but the images noise was still present and loss of color information after thresholding made it even more difficult to remove the noise.

		2. We assumed that each character had a uniform color so, we could just find some top frequency pixels and extract those particular colors. This technique failed because the assumption was invalid. The images had a gradient of color rather than a uniform color.

		3. To remove the effect of the gradient, we just mapped the color 0-255 to 0-8 by dividing each color by 32. The colors thus obtained were free from gradient but this led to loss of some color information. From this reduced image, we calculated the frequency of each of 8*8*8 color and picked up top 10 frequency colors hoping that 
		frequency of pixels of characters have higher frequency than most of random noise. After this the corresponding pixels are extracted from the image and a binary image is procuced. 

		4. Similar to this the characters are extracted for template. During testing the characters are matched pixelwise to all the template characters. A score is calculated from the matching. These characters are also binary image. Sliding window algorithm is implemented which calculates the score for each character over each window and we just store the maximum value of the score. We tried different methods for calculating the score like Intersection Over Union (IOU), Intersection over origenal.

		5. The score can is fed to a multiclass classification algorithm  that predicts which character is present in this image.